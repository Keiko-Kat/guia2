#include <string>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "Contenedor.h"
using namespace std;


#ifndef _PILAS_H_
#define _PILAS_H_

class Pilas{
	//Primero se define la clase Pilas//
	private:	
	//Luego se definen las caracteristicas privadas de Persona//
	//Las que se inicializan con valores neutros//
		int tope = 0;
		bool band;
		int max = 0;
		Contenedor **pila;
	public:
	//se crean dos constructores//
	//Uno por defecto y uno que recibe tres parametros// 
		Pilas();
		Pilas(int tope, bool band, int max);
		
	//Se crean los metodos definidos por los algoritmos entregados en el laboratorio 2//
		void pila_vacia(int tope, bool band);
		
		void pila_llena(int tope, int max, bool band);
		
		void push(Pilas *pila, int tope, int max, Contenedor *dato, int &i, bool band);
		
		void pop(Contenedor *pila, int tope, bool band);
		
	//Se definen los metodos set, que reciben parametros ingresados por el usuario//
		void set_tope(int tope);
		void set_band(bool band);
		void set_max(int max);
		Contenedor set_pila(int max);
			
	//Y se definen metodos get, que retornan los parametros pertenecientes al objeto tipo Pilas//
		int get_tope();
		int get_max();
		bool get_band();
		Contenedor *get_pilas();
		Contenedor *see_pilas(int i);
		
	//print_pila se encarga de imprimir los contenidos de la pila creada//
		void print_pila(Contenedor *pila[], int tope);
		
	
		
};
#endif
