#include <string>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "Pilas.h"
#include "Contenedor.h"
using namespace std;

Pilas::Pilas(){}

//El contructor Pilas recibe tres parametros entregados por el usuario//
Pilas::Pilas(int tope, bool band, int max){
	//Y estos parametros se insertan en las variables correspondientes//
	this->tope = tope;
	this->band = band;
	this->max = max;
	this->pila[max];
	
}
//pila_vacia revisa si la pila posee objetos en su interior//
void Pilas::pila_vacia(int tope, bool band){
	//Esto se realiza revisando el valor de tope//
	//si tope es igual a 0 band se torna true//
	if (tope == 0){
		this->band = true;
	}else{
		//si tope es mayor que 0 band se torna false//
		this->band = false;
	}
}
//pila_llena revisa si queda espacio que llenar en la pila//
void Pilas::pila_llena(int tope, int max, bool band){
	//Esto se realiza revisando el valor de tope//
	//si tope es igual a max band se torna true//
	if(tope == max){
		this->band = true;
	}else{
		//si tope es menor que max band se torna false//
		this->band = false;
	}
}
//push inserta un dato ingresado por el usuario en la pila//
void Pilas::push(Pilas *pila, int tope, int max, Contenedor *dato, int &i, bool band){
	//Si no hay espacio, se muestra un mensaje de error//
	if(band == true){
		cout<<"Desbordamiento, Pila "<<i+1<<" llena"<<endl;
	}else{
		//Si hay espacio se inserta el dato en la posicion tope de pila[]//
		pila->see_pilas(tope)->set_empresa(dato->get_empresa());
		pila->see_pilas(tope)->set_numero_s(dato->get_numero_s());
		//Y se suma 1 a tope//
		tope++;
	}
}
//pop quita el ultimo dato ingresado a la pila//
void Pilas::pop(Contenedor *pila, int tope, bool band){
	//Si no hay elementos se muestra un mensaje de error//
	if(band == true){
		cout<<"Subdesbordamiento, Pila vacía"<<endl;
	}else{
		//Si existen elementos el marcaor tope disminuye en 1//		
		tope--;
	}
}

//El metodo set de tope recibe un int entregado por el usuario//
void Pilas::set_tope(int tope){
	//El cual se inserta en la caracteristica tope del objeto Pilas//
	this->tope = tope;
}
//El metodo set de band recibe un bool entregado por el usuario//
void Pilas::set_band(bool band){
	//El cual se inserta en la caracteristica band del objeto Pilas//
	this->band = band;
}
//El metodo set de max recibe un int entregado por el usuario//
void Pilas::set_max(int max){
	//El cual se inserta en la caracteristica max del objeto Pilas//
	this->max = max;
}
Contenedor Pilas::set_pila(int max){
	this->pila[max];
}
//Los metodos get, extraen la informacion almacenada en el objeto Pilas//
//Para que el usuario pueda accesarlos//
int Pilas::get_tope(){
	return this->tope;
}
int Pilas::get_max(){
	return this->max;
}
bool Pilas::get_band(){
	return this->band;
}
Contenedor *Pilas::get_pilas(){
	return *pila;
}
Contenedor *Pilas::see_pilas(int i){
	return this->pila[i];
}


//print_pila muestra por pantalla el contenid de la pila//
void Pilas::print_pila(Contenedor *pila[], int tope){
	//Se imprime una linea de guiones para mostrar una separacion//
	cout<<"-------------"<<endl;
	//Se abre un ciclo for que recorre el array desde el ultimo elemento hasta el primero//
	for(int i = tope-1; i >= 0; i--){
		//Se imprime cada dato dentro de pila//
		cout<<" ["<<pila[i]->get_numero_s()<<"/"<<pila[i]->get_empresa()<<"]"<<endl;
	}
	//Y finalmente se imprime una linea de guiones para mostrar una separacion//
	cout<<"-------------"<<endl;
}



