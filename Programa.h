#include <string>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "Pilas.h"
#include "Contenedor.h"
using namespace std;

#ifndef _GUIA_H_
#define _GUIA_H_

class Guia{
	
	public: 
		
		void make_port(int max, int size, bool band, int tope, Pilas *puerto[]){
			for(int i = 0; i < size; i++){
				puerto[i] = new Pilas(tope, band, max);
			}
		}
		
		void print_port(int size, Pilas *puerto[], int max){
			for(int i = max -1; i >= 0; i--){
				for (int j = 0; j < size; j++){
					if(puerto[j]->get_tope() > i){
						cout<<" [------] |";
					}else{
						if(puerto[j]->get_tope() <= i){
							cout<<" ["<<puerto[j]->see_pilas(i)->get_numero_s()<<"/"<<puerto[j]->see_pilas(i)->get_empresa()<<"] |";
						}
					}
				}
				cout<<endl;
			}
		}		
		
		//catch_string se asegura de evitar un error de conversion//
		//Asegurando que solo existan numeros en el string ingresado//
		string catch_string(string in){
			//Esta funcion recibe un string ingresado por teclado en la funcion principal//
			//para validar la entrada de un int primero se crea y define is_true como verdadero por default//
			bool is_true = true;
			//Se abre un ciclo while que corre mientras is_true sea verdadero//
			while (is_true == true){
				//Dentro del while se abre un ciclo for que analiza el string recibido//
				for(int j = 0; j < in.size(); j++){
					//Se recorre y se comparan los valores ASCII contenidos en el string//
					//A los valores ASCII de los numros del 0-9 y al valor de '-'//
					if((in[j] >= 48 && in[j] <= 57) || in[j] == 45){
						//si el contenido del string coincide con un valor ASCII numerico o de "-" is_true se iguala a falso//
						is_true = false;
					}else{
						//Si el contenido del string si coincide, is_true se iguala a verdadero y se rompre el ciclo//
						is_true = true;
						break;
					}
				}
				//Luego si se encuentra algo que no es un numero o '-' en el string ingresado//
				if(is_true == true){
					//Se entrega un mensaje de error//
					cout<<"Por favor ingrese valores numéricos:"<<endl;
					//Y se pide otro ingreso//
					getline(cin, in);
				}
			}
			//De esta forma se asegura que la funcion 'stoi()' no entregue error//
			return in;
		}
		
		
		//size_in recibe del usuario el dato max y comprueba que sea un int//
		int size_in(){
			//Se define una variable string que recibe un ingreso de teclado//
			//Y una variable int que recibe el valor y lo retorna a la funcion principal//
			string in;
			int out;
			//Luego se le pide al usuario ingresar el maximo//
			cout<<"Ingrese la cantidad maxima de pilas:"<<endl;	
			//Se recibe como ingreso de teclado//
			getline(cin, in);
			//Y se comprueba que sea un valor completamene numerico//
			in = catch_string(in);
			//Se transforma en un int//
			out = stoi(in);
			//Y se retorna a la funcion principal//
			return out;
		}
		
		//max_in recibe del usuario el dato max y comprueba que sea un int//
		int max_in(){
			//Se define una variable string que recibe un ingreso de teclado//
			//Y una variable int que recibe el valor y lo retorna a la funcion principal//
			string in;
			int out;
			//Luego se le pide al usuario ingresar el maximo//
			cout<<"Ingrese el tamaño maximo de las pilas:"<<endl;	
			//Se recibe como ingreso de teclado//
			getline(cin, in);
			//Y se comprueba que sea un valor completamene numerico//
			in = catch_string(in);
			//Se transforma en un int//
			out = stoi(in);
			//Y se retorna a la funcion principal//
			return out;
		}
		//push_int recibe un dato del usuario y comprueba que sea un int//
		Contenedor *push_cont(){
			//Se define una variable string que recibe un ingreso de teclado//
			//Y una variable int que recibe el valor y lo retorna a la funcion principal//
			string in;
			int num;
			//Luego se le pide al usuario que ingrese un numero//
			cout<<"Ingrese el numero del contenedor que desea insertar:"<<endl;	
			//Se recibe como ingreso de teclado//
			getline(cin, in);
			//Y se comprueba que sea un valor completamene numerico//
			in = catch_string(in);
			//Se transforma en un int//
			num = stoi(in);
			//Y se retorna a la funcion principal//
			
			cout<<"Ingrese la empresa del contenedor que desea insertar:"<<endl;
			//Se recibe como ingreso de teclado//
			getline(cin, in);	
			
			Contenedor *out = new Contenedor(in, num);
			
			return out;
		}
		
		void true_push(Pilas *puerto[], int max, int size, Contenedor *dato, int pop){
			int num = 1;
			int i = 0;
			cout<<"check 1"<<endl;
			for( i = 0; i < size; i++){
				cout<<"check f"<<i<<endl;
				if(pop != i){
					puerto[i]->pila_llena(puerto[i]->get_tope(), max, puerto[i]->get_band());
					cout<<"check 2"<<endl;
					puerto[i]->push(puerto[i], puerto[i]->get_tope(), max, dato, i, puerto[i]->get_band()); 
					cout<<"check 3"<<endl;
					if(puerto[i]->get_band() == false){
						break;
					}
				}
				
				cout<<"check endf"<<endl;
				num++;
			}
			if(num == size){
				cout<<"Puerto lleno, movimiento imposible"<<endl;
				puerto[pop]->set_band(false);
			}else{
				puerto[pop]->set_band(true);
			}
			
			print_port(size, puerto, max);
		} 
		
		void true_pop(Pilas *puerto[], int max, int size){
			int if_there = 0, coor_puer, coor_pil;
			string out;
			cout<<"Que numero de contenedor desea extraer? ";
			getline(cin, out);
			//Y se comprueba que sea un valor completamene numerico//
			out = catch_string(out);
			int out_n = stoi(out);
			for(int j = 0; j < size; j++){
				for(int i = 0; i < max; i++){
					if(puerto[j]->see_pilas(i)->get_numero_s() == out_n){
						//se guardan las "coordenadas" de el objeto//
						coor_puer = j;
						coor_pil = i;
						if_there = 1;	
						break;
					}
				}		
				if(if_there == 1){
					break;
				}		
			}
			//si el contenedor no se encuentra en el puerto se le avisa al usuario//
			if(if_there == 0){
				cout<<"El contenedor "<<out<<" no se encuentra"<<endl;
				
			}else{
				Pilas *pila = new Pilas(); 
				pila = puerto[coor_puer];
				if(coor_pil < pila->get_tope()){
					for(int k = pila->get_tope(); k > coor_pil; k--){
						true_push(puerto, max, size, pila->see_pilas(k), coor_puer);
						if(puerto[coor_puer]->get_band() == true){
							cout<<"Por favor intente nuevamente"<<endl;
							break;
						}else{
							puerto[coor_puer]->pop(puerto[coor_puer]->get_pilas(), puerto[coor_puer]->get_tope(), puerto[coor_puer]->get_band());
							print_port(size, puerto, max);
						} 
					}
					puerto[coor_puer]->pop(puerto[coor_puer]->get_pilas(), puerto[coor_puer]->get_tope(), puerto[coor_puer]->get_band());
					print_port(size, puerto, max);
				}
			}
		}
						 
					
				 
		
		
		//La funcion menu se utiliza para decidir que hacer en el programa//
		int menu(){
			//Se define una variable string que recibe un ingreso de teclado//
			//Y una variable int que recibe el valor y lo retorna a la funcion principal//
			string in;
			int temp;
			cout<<endl;
			//Luego se imprimen las opciones que se le dan al usuario en pantalla//
			cout<<"Agregar/Push           [1]"<<endl;
			cout<<"Retirar/Pop            [2]"<<endl;
			cout<<"Ver Puerto             [3]"<<endl;
			cout<<"Cerrar programa        [0]"<<endl;
			cout<<"--------------------------"<<endl;
			cout<<"--------------------------"<<endl;
			cout<<"Opción:  ";
			//Para luego pedir que ingrese la opcion que desea realizar//
			//Se recibe como ingreso de teclado//
			getline(cin, in);
			//Y se comprueba que sea un valor completamene numerico//
			in = catch_string(in);
			//Para luego transformarlo a una variable int//		
			temp = stoi(in);
			//Y retornar dicho int como eleccion a la funcion principal//
			return temp;
		}	
			
		//run contiene todas las instrucciones para correr el programa//
		void run(){
			//Se crea el int temp, para mantener el programa corriendo//
			int temp = -1;
			//Y se crean tres ints mas para guardar las informaciones necesarias//
			int tope = 0, max, size;
			Contenedor *data = new Contenedor();
			cout<<"check 1"<<endl;
			//Y una variable bool que permitira definir los pasos posibles al llenar y vaciar la pila//
			bool band = true;
			max = max_in();
			size = size_in();
			Pilas *puerto[size];
			make_port(max, size, band, tope, puerto);
			
			//Y se abre un while que correra mientras temp sea distinto a 0//
			while (temp != 0){
				//Luego se llama a menu para decidir que hara el programa//
				temp = menu();		
				//Y en base a temp se abre una condicional switch//
				switch(temp){
					//En caso de que temp sea 1, se llama a push int para recibir un dato del usuario//
					case 1: data = push_cont();
							//Y se inserta en la pila llamando a push//
							true_push(puerto, max, size, data, -1); 
							break;
					//En caso de que temp sea 2, se llama a pop para eliminar el ultimo dato ingresado//
					case 2: true_pop(puerto, max, size);
							break;
					//En caso de que temp sea 3, se llama a print_port para imprimir los contenidos de pila//
					case 3: print_port(size, puerto, max);
							break;
					//En caso de que temp sea 0, el programa se salta el switch y se cierra el ciclo while//
					case 0: 
							break;
					//En caso de que temp no sea ninguna de las 4 opciones anteriores se envia un mensaje de error//
					default: cout<<"opcion no valida, intente de nuevo"<<endl;
							//Y se reinicia el while//
							break;	
				}	
			}
		}

};
#endif
