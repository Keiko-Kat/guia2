#include <string>
#include <list>
#include <iostream>
using namespace std;
#include "Contenedor.h"


Contenedor::Contenedor(){}

Contenedor::Contenedor(string empresa, int numero_s){
	this->empresa = empresa;
	this->numero_s = numero_s;
}

void Contenedor::set_empresa(string empresa){
	this->empresa = empresa;
}
void Contenedor::set_numero_s(int numero_s){
	this->numero_s = numero_s;
}
string Contenedor::get_empresa(){
	return this->empresa;
}
int Contenedor::get_numero_s(){
	return this->numero_s;
}
