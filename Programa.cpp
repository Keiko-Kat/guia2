#include <string>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "Pilas.h"
#include "Contenedor.h"
using namespace std;
#include "Programa.h"

int main(){
	//Se crea el objeto tipo Guia para correr el programa//
	Guia *puerto = new Guia();
	//Se llama a la funcion run de Guia//
	//run() contiene las instrucciones necesarias para funcionar//
	puerto->run();
	//Se retorna 0 para acabar el main//
	
	return 0;
}
